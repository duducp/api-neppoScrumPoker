// https://tableless.com.br/criando-uma-aplicacao-de-chat-simples-com-nodejs-e-client-io/

'use strict';
const moment = require('moment');
moment.locale('pt-BR');

var teamOnline = [];
var roomManagersStartedVoting = [];
var votingStoryStarted = [];

exports.socketServer = function (sockets) {
    const io = sockets;

    const projects = io
    .of('/projects')
    .on('connection', function (client) {
        var room = client.handshake.query.idProject;
        var nameUser = client.handshake.query.nameUserConnected;
        var idUser = client.handshake.query.idUserConnected;
        var departmentUser = client.handshake.query.departmentUserConnected;
        var userConnectedIsTeam = client.handshake.query.userConnectedIsTeam;

        // evento do usuário que conecta
        client.on('connected', function () {
            client.join(room, function () {
                 // Separo somente o time do projeto que o usuário entrou
                var teamOnlineRoom = [];
                for (var i = 0; i < teamOnline.length; i++) {
                    if (room == teamOnline[i].room) {
                        teamOnlineRoom.push(teamOnline[i]);
                    }
                }

                // Adiciono o usuário ao TeamOnline
                var user = {};
                user = {
                    _id: idUser,
                    name: nameUser,
                    department: departmentUser,
                    idSocket: client.id,
                    room: room
                };

                // Verifico se está avendo alguma votação de estória
                var votingStarted = false;
                for (var i = 0; i < roomManagersStartedVoting.length; i++) {
                    if (roomManagersStartedVoting[i].room == room) {
                        votingStarted = true;
                        break;
                    }
                }

                // Verifica se o usuário que entrou no projeto pertence a equipe
                if (userConnectedIsTeam == 'true') {
                    teamOnline.push(user);

                    // envia para todos os clientes (com exceção do atual) para uma sala específica
                    client.broadcast.to(room).emit('userConnected', user);
                }

                 // envia apenas para o cliente atual
                client.emit('userConnectedTeamOnline', teamOnlineRoom, user, votingStarted);

                // Atualiza a lista de usuários online caso haja alguém visualizando-a
                projects.in(room).emit('getTeamOnline', teamOnline);

                 // envia para todos os clientes (com exceção do atual) para uma sala específica
                client.broadcast.to(room).emit('notification', {nameUser: nameUser, action: "enter", date: moment().format()});
            });
        });

        client.on('getTeamOnline', function () {
            client.emit('getTeamOnline', teamOnline);
        });

        // Comunica o início da votação da estória
        client.on('startVoting', function (res, idSocket) {
            var data = {
                idSocket: client.id,
                room: room
            };
            roomManagersStartedVoting.push(data);

            projects.in(room).emit('startVoting', res);
        });

        // Comunica que é para virar as cartas
        client.on('turnCards', function () {
            for (var i = 0; i < teamOnline.length; i++) {
                delete teamOnline[i].valueVotedCard;
            }

            projects.in(room).emit('turnCards');
        });

        // Comunica que um usuário votou na estória
        client.on('votedCard', function (cardVoted) {
            var data = {
                voted: cardVoted,
                idSocket: client.id
            };

            for (var i = 0; i < teamOnline.length; i++) {
                if (teamOnline[i].idSocket == client.id && teamOnline[i].room == room) {
                    teamOnline[i]['valueVotedCard'] = cardVoted;
                    break;
                }
            }

            projects.in(room).emit('votedCard', data);
        });

        // Comunica que é para reiniciar a votação
        client.on('restartVoting', function () {
            for (var i = 0; i < teamOnline.length; i++) {
                delete teamOnline[i].valueVotedCard;
            }

            projects.in(room).emit('restartVoting');
        });

        // Comunica que a votação de uma estória foi cancelada
        client.on('cancelVoting', function () {
            for (var i = 0; i < teamOnline.length; i++) {
                delete teamOnline[i].valueVotedCard;
            }

            projects.in(room).emit('cancelVoting');
        });

        // Comunica a Carta Final escolhida pelo Admin
        client.on('finalCardStory', function (card) {
            function ArrayElements(element, index, array) {
                if (element.idSocket === client.id) {
                    roomManagersStartedVoting.splice(index, 1);
                }
            }
            roomManagersStartedVoting.forEach(ArrayElements);

            projects.in(room).emit('finalCardStory', card);
        });

        // Comunica sobre nova notificação de ação do usuário
        client.on('notification', function (data) {
            projects.in(room).emit('notification', data);
        });

        // Comunica sobre nova mensagem
        client.on('messages', function (data) {
            client.broadcast.to(room).emit('messages', data);
        });

        // Comunica qual usuario entrou na conversa do projeto
        client.on('enterExitConversation', function (data) {
            client.broadcast.to(room).emit('enterExitConversation', data);
        });

        // evento do usuário que é desconectado
        client.on('userDesconnected', function () {
            client.disconnect();
        });

        client.on('disconnect', function () {
            // Verifica se o admin da sala deixou-a e cancela a votação, caso tenha iniciado
            for (var i = 0; i < roomManagersStartedVoting.length; i++) {
                if (roomManagersStartedVoting[i].idSocket === client.id) {
                    roomManagersStartedVoting.splice(i, 1);
                    projects.in(room).emit('cancelVoting');
                    break;
                }
            }

            // Informa qual usuário deixou a sala
            function ArrayElements(element, index, array) {
                if (element.idSocket === client.id) {
                    teamOnline.splice(index, 1); // remove do array o usuário que saiu
                    client.leave(room); // remove o client da room

                    projects.in(room).emit('userDesconnected', client.id);
                    projects.in(room).emit('getTeamOnline', teamOnline);
                    projects.in(room).emit('notification', {nameUser: element.name, action: "exit", date: moment().format()});
                }
            }
            teamOnline.forEach(ArrayElements);
        });

    });
};
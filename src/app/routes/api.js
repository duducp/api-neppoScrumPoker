'use strict';
const express = require('express'),
    passportService = require('../../config/passport'),
    passport = require('passport');

const AuthenticationController = require('../controllers/authentication'),
    UserController = require('../controllers/user'),
    ProjectController = require('../controllers/project'),
    StoryController = require('../controllers/story'),
    StoryVotedController = require('../controllers/storyVoted');

const requireAuth = passport.authenticate('jwt', {session: false}),
    requireLogin = passport.authenticate('local', {session: false});

const sockets = require('./sockets');

module.exports = function (app, io) {

    const versionApi = express.Router(),
        apiRoutes = express.Router(),
        authRoutes = express.Router(),
        userRoutes = express.Router(),
        projectRoutes = express.Router(),
        storyRoutes = express.Router(),
        storyVotedRoutes = express.Router();

    // Executa o real Time
    sockets.socketServer(io);

    // Set up routes
    app.use('/api', versionApi);

    // Api V1
    versionApi.use('/v1',
        // Home
        authRoutes.get('/', function (req, res) {
            //res.send({content: 'Success'});
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('Hello there, world!\n');
        }),

        // Auth Routes
        apiRoutes.use('/auth',
            authRoutes.post('/register', AuthenticationController.register),
            authRoutes.post('/login', requireLogin, AuthenticationController.login)
        ),

        // User Routes
        apiRoutes.use('/user',
            userRoutes.get('/', requireAuth, UserController.getAllUsers),
            userRoutes.get('/:id_user', requireAuth, UserController.getUser)
        ),

        // Project Routes
        apiRoutes.use('/project',
            projectRoutes.get('/', requireAuth, ProjectController.getAllProjects),
            projectRoutes.get('/:id_project', requireAuth, ProjectController.getProject),
            projectRoutes.post('/', requireAuth, ProjectController.create),
            projectRoutes.put('/:id_project', requireAuth, ProjectController.update),
            projectRoutes.delete('/:id_project', requireAuth, ProjectController.delete)
        ),

        // Story Routes
        apiRoutes.use('/story',
            storyRoutes.get('/:id_project/:id_story', requireAuth, StoryController.getStory),
            storyRoutes.get('/:id_project', requireAuth, StoryController.getAllStoryProject),
            storyRoutes.post('/:id_project', requireAuth, StoryController.create),
            storyRoutes.put('/:id_project/:id_story', requireAuth, StoryController.update),
            storyRoutes.delete('/:id_project/:id_story', requireAuth, StoryController.delete)
        ),

        apiRoutes.use('/storys-voted',
            storyVotedRoutes.get('/:id_project', requireAuth, StoryVotedController.getStoriesVoted),
            storyVotedRoutes.post('/:id_project/:id_story', requireAuth, StoryVotedController.create),
            storyVotedRoutes.delete('/:id_project/:id_story', requireAuth, StoryVotedController.deleteAllVotingStoryProject)
        )
    );
};
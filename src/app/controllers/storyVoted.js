'use strict';
const mongoose = require('mongoose');
const moment = require('moment');
moment.locale('pt-BR');

const storyVoted = require('../models/storyVoted');
const story = require('../models/story');

function sortAndOrderBy(sort, orderBy) {
    if (sort) {
        sort = sort.split(' ');
    } else {
        sort = 'asc';
    }

    if (orderBy) {
        orderBy = orderBy.split(' ');
    } else {
        orderBy = 'createdAt';
    }
        
    var sortObj = {};
    for (var i=0; i < orderBy.length; i++) {
        if (sort[i] !== 'asc' || sort[i] == 1) {
            sort = 1; //desc
        } else {
            sort = -1; //asc
        }

        sortObj[orderBy[i]] = sort;
    }

    return sortObj;
}

function select(key, text) {
    var obj = {};

    if (key && text) {
        key = key.split(' ');
        text = text.split(' ');

        for (var i=0; i < key.length; i++) {
            if (key[i] == "id_user" || key[i] ==  "_id" || key[i] ==  "id_project") {
                obj[key[i]] = mongoose.Types.ObjectId(text[i]);
            } else {
                obj[key[i]] = text[i]; //select key=text
            }
        }
    }

    return obj;
}

exports.create = function (req, res, next) {
    storyVoted.findOneAndUpdate(
        {$and: [
            {id_project: req.params.id_project},
            {id_story: req.params.id_story},
            {id_user: req.user._id}
        ]},
        {$set: {note: req.body.note, time_voting: req.body.time_voting}},
        {new: true, upsert: true},
        function(err, doc){
            if(err)
                return res.status(500).send({message: 'Erro ao atualizar a carta', error: err});

            return res.status(200).json({
                message: 'Carta salva com sucesso',
                storyVoting: doc
            });
        }
    );
};

exports.deleteAllVotingStoryProject = function (req, res, next) {
    storyVoted.remove({$and: [
        {id_project: req.params.id_project},
        {id_story: req.params.id_story}
    ]}, function (err, deleted) {
        if (err)
            return res.status(500).send({message: 'Erro ao excluir as cartas votadas na estória', error: err});

        return res.status(200).json({message: 'Todas as cartas votadas na estória, foram excluídas'});
    });
};

exports.getStoriesVoted = function (req, res, next) {
    //Parametros de pesquisa
    const sortObj = sortAndOrderBy(req.query.sort, req.query.orderBy);
    const selectObj = select(req.query.key, req.query.text)

    var search;
    if (Object.keys(selectObj).length === 0) {
        search = {id_project: mongoose.Types.ObjectId(req.params.id_project)};
    } else {
        search = {$and: [{id_project: mongoose.Types.ObjectId(req.params.id_project)}, selectObj]};
    }

    // Prepara a pesquisa
    if (req.query.orderUsers == 'true') {
        var query = story.aggregate([
            {$match: search},
            {$sort: sortObj},

            {$lookup: {
                from: "storyvoteds",
                localField: "_id",
                foreignField: "id_story",
                as: "story"
            } },
            {$unwind: {path: "$story", preserveNullAndEmptyArrays: true}},

            {$lookup: {
                from: "users",
                localField: "story.id_user",
                foreignField: "_id",
                as: "user"
            } },
            {$unwind: {path: "$user", preserveNullAndEmptyArrays: true}},

            {$lookup: {
                from: "projects",
                localField: "story.id_project",
                foreignField: "_id",
                as: "project"
            } },
            {$unwind: {path: "$project", preserveNullAndEmptyArrays: true}},

            {
                $group: {
                    _id: "$user._id",
                    name: {$first: "$user.name"},
                    stories: {
                        $addToSet: {
                            _id: "$_id",
                            title: "$title",
                            description: "$description",
                            time_total: "$time_total",
                            card_final: "$card_final",
                            note: "$story.note",
                            time_voting: "$story.time_voting",
                        }
                    },
                    project: {
                        $addToSet: {
                            _id: "$project._id",
                            title: "$project.title",
                            description: "$project.description",
                        }
                    }
                }
            }
        ]);
    } else {
        var query = story.aggregate([
            {$match: search},
            {$sort: sortObj},

            {$lookup: {
                from: "storyvoteds",
                localField: "_id",
                foreignField: "id_story",
                as: "story"
            } },
            {$unwind: {path: "$story", preserveNullAndEmptyArrays: true}},

            {$lookup: {
                from: "users",
                localField: "story.id_user",
                foreignField: "_id",
                as: "user"
            } },
            {$unwind: {path: "$user", preserveNullAndEmptyArrays: true}},

            {$lookup: {
                from: "projects",
                localField: "story.id_project",
                foreignField: "_id",
                as: "project"
            } },
            {$unwind: {path: "$project", preserveNullAndEmptyArrays: true}},

            {
                $group: {
                    _id: "$_id",
                    title: {$first: "$title"},
                    description: {$first: "$description"},
                    time_total: {$first: "$time_total"},
                    card_final: {$first: "$card_final"},
                    users_voted: {
                        $addToSet: {
                            _id: "$user._id",
                            name: "$user.name",
                            note: "$story.note",
                            time_voting: "$story.time_voting"
                        }
                    },
                    project: {
                        $addToSet: {
                            _id: "$project._id",
                            title: "$project.title",
                            description: "$project.description",
                        }
                    }
                }
            }
        ]);
    }
    
    query.exec(function (err, project) {
        if (err)
            return res.status(500).send({message: 'Erro de servidor', error: err});

        if (!project || !project.length)
            return res.status(422).send({message: 'Nenhum registro de estórias votadas foi encontrado'});

        return res.status(200).send(project);
    });
};

exports.getUsersVotedStory = function (req, res, next) {
    //Parametros de pesquisa
    const sortObj = sortAndOrderBy(req.query.sort, req.query.orderBy);
    const selectObj = select(req.query.key, req.query.text)

    var search;
    if (Object.keys(selectObj).length === 0) {
        search = {id_project: mongoose.Types.ObjectId(req.params.id_project)};
    } else {
        search = {$and: [{id_project: mongoose.Types.ObjectId(req.params.id_project)}, selectObj]};
    }

    // Prepara a pesquisa
    var query = story.aggregate([
        {$match: search},
        {$sort: sortObj},

        {$lookup: {
            from: "storyvoteds",
            localField: "_id",
            foreignField: "id_story",
            as: "story"
        } },
        {$unwind: {path: "$story", preserveNullAndEmptyArrays: true}},

        {$lookup: {
            from: "users",
            localField: "story.id_user",
            foreignField: "_id",
            as: "user"
        } },
        {$unwind: {path: "$user", preserveNullAndEmptyArrays: true}},

        {
            $group: {
                _id: "$user._id",
                name: {$first: "$user.name"},
                stories: {
                    $addToSet: {
                        title: "$title",
                        description: "$description",
                        time_total: "$time_total",
                        card_final: "$card_final",
                        note: "$story.note",
                        time_voting: "$story.time_voting",
                    }
                }
            }
        }
    ]);

    query.exec(function (err, project) {
        if (err)
            return res.status(500).send({message: 'Erro de servidor', error: err});

        if (!project || !project.length)
            return res.status(422).send({message: 'Nenhum registro de estórias votadas foi encontrado'});

        return res.status(200).send(project);
    });
};
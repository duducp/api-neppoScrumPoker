'use strict';
const mongoose = require('mongoose');
const moment = require('moment');
moment.locale('pt-BR');

const Project = require('../models/project');
const Story = require('../models/story');
const StoryVoted = require('../models/storyVoted');

function sortAndOrderBy(sort, orderBy) {
    if (sort) {
        sort = sort.split(' ');
    } else {
        sort = 'asc';
    }

    if (orderBy) {
        orderBy = orderBy.split(' ');
    } else {
        orderBy = 'createdAt';
    }
        
    var sortObj = {};
    for (var i=0; i < orderBy.length; i++) {
        if (sort[i] !== 'asc' || sort[i] == 1) {
            sort = 1; //desc
        } else {
            sort = -1; //asc
        }

        sortObj[orderBy[i]] = sort;
    }

    return sortObj;
}

exports.getAllProjects = function (req, res, next) {
    const sortObj = sortAndOrderBy(req.query.sort, req.query.orderBy);

    if (req.query.limit > 0) {
        var limit = req.query.limit;
    } else {
        var limit = 1000;
    }

    if (req.query.skip > 0) {
        var skip = req.query.skip;
    } else {
        var skip = 0;
    }

    var obj = {};
    obj[req.query.key] = req.query.text; //select key=text

    if (req.query.userTeam === 'true')
        obj['$or'] = [{'team': req.user._id}, {'id_user': req.user._id}];

    var query = Project.aggregate([
        {$match: obj},
        {$sort: sortObj},
        {$limit : Number(limit)},        
        {$skip : Number(skip)},     

        {$lookup: {
            from: "stories",
            localField: "_id",
            foreignField: "id_project",
            as: "project_story"
        } },
        {$unwind: {path: "$project_story", preserveNullAndEmptyArrays: true}},

        {
            $group: {
                _id: "$_id",
                title: {$first: "$title"},
                description: {$first: "$description"},
                createdAt: {$first: "$createdAt"},
                date_start: {$first: "$date_start"},
                start_in_date: {$first: "$start_in_date"},
                date_end: {$first: "$date_end"},
                current_story: {$first: "$current_story"},
                cards: {$first: "$cards"},
                team: {$first: "$team"},
                id_user: {$first: "$id_user"},
                total_story: {
                    $sum: {
                        $cond: [ "$project_story", 1, 0 ]
                    }
                },
                total_story_voted: {
                    $sum: {
                        $cond: [ {
                            $and: ["$project_story.card_final", "$project_story.time_total"]
                        }, 1, 0 ]
                    }
                }
            }
        }
    ]);

    query.exec(function (err, project) {
        if (err)
            return res.status(500).send({message: 'Erro de servidor', error: err});

        if (!project || !project.length)
            return res.status(422).send({message: 'Nenhum projeto foi encontrado'});

        return res.status(200).send(project);
    });
};

exports.getProject = function (req, res, next) {
    var obj = {};
    obj['_id'] = mongoose.Types.ObjectId(req.params.id_project);

    if (req.query.userTeam === 'true')
        obj['$or'] = [{'team': req.user._id}, {'id_user': req.user._id}];

    var query = Project.aggregate([
        {$match: obj},

        {$lookup: {
            from: "users",
            localField: "id_user",
            foreignField: "_id",
            as: "id_user"
        } },
        {$unwind: {path: "$id_user", preserveNullAndEmptyArrays: true}},

        {$lookup: {
            from: "stories",
            localField: "current_story",
            foreignField: "_id",
            as: "current_story"
        } },
        {$unwind: {path: "$current_story", preserveNullAndEmptyArrays: true}},

        {$unwind: "$team"},
        {$lookup: {
            from: "users",
            localField: "team",
            foreignField: "_id",
            as: "team"
        } },
        {$unwind: {path: "$team", preserveNullAndEmptyArrays: true}},

        {
            $group: {
                _id: "$_id",
                title: {$first: "$title"},
                description: {$first: "$description"},
                createdAt: {$first: "$createdAt"},
                date_start: {$first: "$date_start"},
                start_in_date: {$first: "$start_in_date"},
                date_end: {$first: "$date_end"},
                cards: {$first: "$cards"},
                team: {
                    $addToSet: {
                        _id: "$team._id",
                        name: "$team.name",
                        email: "$team.email",
                        department: "$team.department",
                        role: "$team.role",
                        profilePicture: "$team.profilePicture"
                    }
                },
                id_user: {
                    $addToSet: {
                        _id: "$id_user._id",
                        name: "$id_user.name",
                        email: "$id_user.email",
                        department: "$id_user.department",
                        role: "$id_user.role",
                        profilePicture: "$id_user.profilePicture"
                    }
                },
                current_story: {
                    $push: "$current_story"
                }
            }
        }
    ]);

    query.exec(function (err, project) {
        if (err)
            return res.status(500).send({message: 'Erro de servidor', error: err});

        if (!project || !project.length)
            return res.status(422).send({message: 'Nenhum projeto foi encontrado'});

        return res.status(200).send(project);
    });
};

exports.create = function (req, res, next) {
    if (req.user.role !== 'admin') {
        res.status(401).send({message: 'Você não está autorizado a cadastrar projeto'});
        return next('Não autorizado');
    }

    Project.create({
        title: req.body.title,
        description: req.body.description,
        date_start: moment(req.body.date_start).utc().format(),
        date_end: req.body.date_end,
        current_story: req.body.current_story,
        cards: req.body.cards,
        team: req.body.team,
        id_user: req.user._id
    }, function (err, project) {
        if (err)
            return res.status(500).send({message: 'Erro ao criar projeto', error: err});

        res.status(201).send({
            message: 'Projeto criado com sucesso',
            project: project
        });
    });
};

exports.update = function (req, res, next) {
    var optionsObj = {
        new: true,
        upsert: true
    };

    Project.findByIdAndUpdate(req.params.id_project, {$set: req.body}, optionsObj, function (err, updateProject) {
        if (err)
            return res.status(500).send({message: 'Erro ao atualizar projeto', error: err});

        if (updateProject.id_user.toString() !== req.user._id.toString() && req.user.role !== 'admin') {
            res.status(401).send({message: 'Você não está autorizado a modificar este projeto'});
            return next('Não autorizado');
        }

        res.status(200).send({
            message: 'Projeto atualizado com sucesso',
            project: updateProject
        });
    });
};

exports.delete = function (req, res, next) {
    const id_project = req.params.id_project;

    Project.findById(id_project, function (err, foundProject) {
        if (err)
            res.status(422).send({message: 'Projeto não encontrado', error: err});

        if (foundProject.id_user.toString() !== req.user._id.toString() && req.user.role !== 'admin') {
            res.status(401).send({message: 'Você não está autorizado a excluir este projeto'});
            return next('Não autorizado');
        }

        Project.remove({_id: id_project}, function (err, deleted) {
            if (err)
                return res.status(500).send({message: 'Erro ao excluir projeto', error: err});

            Story.remove({id_project: id_project}, function (err, deleted) {
                if (err)
                    return res.status(500).send({message: 'Projeto excluído mas com erro ao excluir estória', error: err});

                StoryVoted.remove({id_project: id_project}, function (err, deleted) {
                    if (err)
                        return res.status(500).send({message: 'Projeto e Estórias foram excluídos mas teve erro ao excluir as cartas votadas no projeto.', error: err});

                    return res.status(200).send({message: 'Projeto excluído com sucesso'});
                });
            });
        });
    });
};
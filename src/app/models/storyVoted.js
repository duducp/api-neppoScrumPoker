var mongoose = require('mongoose');

var StoryVotedSchema = new mongoose.Schema({
    id_project: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    id_story: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    id_user: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    note: {
        type: String,
        required: true
    },
    time_voting: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('StoryVoted', StoryVotedSchema);
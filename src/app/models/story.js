var mongoose = require('mongoose');

var StorySchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Informe o título da Estória']
    },
    description: {
        type: String,
        required: [true, 'Informe uma descrição para a Estória']
    },
    id_project: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    id_user: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    time_total: String,
    card_final: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Story', StorySchema);